import requests

# cookies = driver.get_cookies()
cookies = [
    {'domain': '.zhihu.com', 'expiry': 1627998485.311418, 'httpOnly': True, 'name': 'captcha_session_v2', 'path': '/',
     'secure': False,
     'value': '"2|1:0|10:1625406487|18:captcha_session_v2|88:VXdhSkVIZ2tuS3gwa280eWg0RmMwNkF1WDFPZWhwY1BwL3pVeTU0RithcVlCU0ZoREpxclcvRlJFRjdlL25ueQ==|cb33d4f2994f5b010b9d856f34eb6053a67f2ba7e80e326ff0dd341500d295e5"'},
    {'domain': 'www.zhihu.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'YD00517437729195%3AWM_NI',
     'path': '/', 'secure': False,
     'value': '1U2Ky8Bvr7jiqznJLPehkxnIvIDGkldDiJO2W87xSaNZmS0IFSiNOEPOYvkAPTLOXUpB1Yk8g0YRjep%2FIlAzV4l8EjpHGEzj%2Bd0703O5wduYK%2BAeEjS5bTgQ%2FwQl%2FPPNaEU%3D'},
    {'domain': '.zhihu.com', 'expiry': 1627998485.022688, 'httpOnly': True, 'name': 'captcha_ticket_v2', 'path': '/',
     'secure': False,
     'value': '"2|1:0|10:1625406487|17:captcha_ticket_v2|704:eyJ2YWxpZGF0ZSI6IkNOMzFfZ3BFWkRJdUV2TnZXYXlLLW91SnJ0OVZVOS5ZcDc2UVRfVmxpVkp0d2dMTGN4aTk2dXhIclY4dnRUbFhBRFFHZEZuclM1SVE2eFlnQXhKRGJHUkRfN3RIZlBhVU5JOWsuMnFGNmlGaDRrWmlYYktjZU1zLVUtZy44Uy00QS5wNDZteEpUVjhrWVF3blQ1RWNsVEV6N05YR3phQjBXTUI2WnBhTXZyLjgyS0ppdFd3cDRjUFhCZ3dmak1mLlpOVG50aGJkbkdPX3JodHVrVjRjQWNqSkU2cDFsYUpzVEZENDlwbE1ER2xZVXc5YVRpWUpQVkdrTk0wRi01YkVkMVBkVUFZekpKb1RqUFJPWkJVTmxPcGRpaktjLlFCeXgxRXdJXzFjQWZTbDQ4bVgtT25PR3JGb2c0cXlOYy5oUzJIdVRMdV9PYmtxY0dHOTVkWHZkZC1FbGF3SjhrLmh5MnFfVHJkQnNpRGZsTGhBbElUNTk0anE2TVJLT0R5SEZ1RWU0eGpJSmZXUUJkUGRDX3BnaHlnMFdMMVU0QTlINkdra3ZDc2tIVWl2NDEtOVRPZ1N4dS1KMW9MZ21xWjdEZXdpQWJqcTA5S2ljUWNXc2NOLUlPU3FkZ3owa2NpeUVoSFNUazJ3cS1yb0lEYm92S3pQdFdteUJrSnZIai5tMyJ9|9d912c37346caca3c5e4c9924f5969f7637cceab548efad23b83e5b9ffe3f73a"'},
    {'domain': 'www.zhihu.com', 'expiry': 1656942481, 'httpOnly': False, 'name': '__snaker__id', 'path': '/',
     'secure': False, 'value': 'f9zOLvaNOVN2aZcy'},
    {'domain': 'www.zhihu.com', 'expiry': 1783087382, 'httpOnly': False, 'name': '_9755xjdesxxd_', 'path': '/',
     'secure': False, 'value': '32'},
    {'domain': 'www.zhihu.com', 'expiry': 1783087382, 'httpOnly': False, 'name': 'gdxidpyhxdE', 'path': '/',
     'secure': False,
     'value': 'eaIJAYuT%2FTRU0m6Vq2R1EZU5uX%5CuZbCSo06ls%5Ch72arfKEYsfJY8pXXhvJCKTPtgRxOzYmmNIr54NWD0eTmUvPYvdsaOKPZnqN680kax9GH6S26f1aPvn7p%5C92o4Zrxvh%2Fi%2FcpwTJfyuiqNjGgXim3t8uCotLQe8M132qvNL9rIXeLEW%3A1625407382342'},
    {'domain': 'www.zhihu.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'YD00517437729195%3AWM_TID',
     'path': '/', 'secure': False, 'value': 'HHFlNPk4HyBFFBVBVBJ%2FiHtXsH3PXnSr'},
    {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'JOID', 'path': '/', 'secure': False,
     'value': 'U1sRC0LkijeyF6GVNO1ALtxMLcMij8Zs5ybs3XGJuFTYccX6V_I929sdpJ45oM9yKni7D1IqaW3EVKpHFmHNMrY='},
    {'domain': '.zhihu.com', 'expiry': 1656942480, 'httpOnly': False, 'name': 'Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49',
     'path': '/', 'secure': False, 'value': '1625406481'},
    {'domain': '.zhihu.com', 'expiry': 1720014480.723885, 'httpOnly': False, 'name': 'd_c0', 'path': '/',
     'secure': False, 'value': '"AMAehFWFXBOPTu0bdQtGcTAQy3ckpJ_YdfI=|1625406482"'},
    {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'KLBRSID', 'path': '/', 'secure': False,
     'value': 'fe78dd346df712f9c4f126150949b853|1625406487|1625406480'},
    {'domain': '.zhihu.com', 'httpOnly': False, 'name': 'Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49', 'path': '/',
     'secure': False, 'value': '1625406481'},
    {'domain': 'www.zhihu.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'YD00517437729195%3AWM_NIKE',
     'path': '/', 'secure': False,
     'value': '9ca17ae2e6ffcda170e2e6ee9aeb5fedbaa188c552f3868ba2d54b878e9abbb53a91e888d9d23aa38dfea8f12af0fea7c3b92af38bfeacfc3482aabed8e963b3a6a2abcf5ca7b8a599ed43b6978ca8ea68a89ca7abce3a95e8f999d26b8e86a3b4b343868da7afce4f94919797ea4b948c84b0db5fb49effbaea34a5f097b3e96fb4f1878dc47296eca4d9c862b49889a6f962e9bc00d8b3398debb985f85389f58f86c86291ebaeb5e65beda99f8dae5c9492afa6d837e2a3'},
    {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'osd', 'path': '/', 'secure': False,
     'value': 'UVgdA07miTu6G6OWOOVMLN9AJc8gjMpk6yTv0XmFulfUecn4VP4119keqJY1osx-InS5DF4iZW_HWKJLFGLBOro='},
    {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'SESSIONID', 'path': '/', 'secure': False,
     'value': 'iKWT9OOIe0dfglmdEa3yuOo6KPhKNAMBjE7SSoxpmRc'},
    {'domain': '.zhihu.com', 'httpOnly': False, 'name': '_xsrf', 'path': '/', 'secure': False,
     'value': '219bd260-952d-4815-b5b7-5a46a59d019e'},
    {'domain': '.zhihu.com', 'expiry': 1688478478.97232, 'httpOnly': False, 'name': '_zap', 'path': '/',
     'secure': False, 'value': '32e465c5-96d0-43ce-8f26-6430b3e06b9c'}]

headers = {

    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36',
    'cookie': '_zap=a360a06a-863b-4cba-b29c-5cd72cf0d127; _xsrf=1f90c97a-53bc-444b-ae9b-48119a07cfea; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1625408351; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1625408351; d_c0="AHBebHiMXBOPTlz164s7LGYGcpC68dYokHU=|1625408353"; captcha_session_v2="2|1:0|10:1625408353|18:captcha_session_v2|88:NnJoZ2V5TVpiSFA1eklGQUM3clEwUlRKdStoRzU2emJpajhzOTZVRmdOQkgvK1ZUTDlIM0pqcjA4MmtUVFQ0cA==|78d054ace1eb42c609ae7c73c6313ca4ad32c40f66479d800879a5bd79620e77"; SESSIONID=e1vfFtp4fPIk2zdlZMJYlbea9JonbhhOdJPvbMJiKXH; JOID=VFwTA0tuiYco8ITIV2g9ml6PDpBECbr3TrDyumUO28xFwviuOQxOYE70hMxRRXAyFqBDZT-9oVWTif9uNXjxjAk=; osd=VV8cBk5viogt9YXLWG04m12AC5VFCrXyS7HxtWAL2s9Kx_2vOgNLZU_3i8lURHM9E6VCZjC4pFSQhvprNHv-iQw=; __snaker__id=Z0Li8ndCCQvYgn84; gdxidpyhxdE=Da2Rdz%2Bb%2F0Dg7ZrO%2FHYc4aZQrwPpKMkAjJ0XI0uVjid1CgtXLLeMVW43njYB1YkvfnJpw4cx0Adfd51Rgp%2FDw8N3Caco4YO3ZbXGxEVvyA4vGNNonGOv3goxyeDnh09V%2Fs6Squ0Od4Z%5Cz1%5C6I%2BtTzyJ60w9UyojX%2BfCvANluyYEXzAHv%3A1625409253954; _9755xjdesxxd_=32; YD00517437729195%3AWM_NI=pWXlPsBhokjGHssEFlBJKsfnM8ww3XWVK5a9LeTEpUg1SsNEZ5qJMDmV%2FW9VjK7fKL%2BO5F%2Buo5dlALI%2BmgbKj8ZO9mkgDLwjaHEzv5AE4vRT6y44atL46oe3CjTurHgrcjE%3D; YD00517437729195%3AWM_NIKE=9ca17ae2e6ffcda170e2e6eeb9c74082ec8b82aa50e9ac8eb2d85a938b8babae6798ad9c92d479bc979ad5c92af0fea7c3b92ab489988dd37982b8a18ce94d8cadbda6c56882968594e5668fe8b998b254958a85d6e43390bb9ca6f35fa5ef9d94cd4f9aebbfabd050af8ae1b8bc5eaa95f987ca48e991fba7aa7fadebac8dc4488bf5a388e141aae896b0bc67a9b98cadf662a3988b9bf53f839bfc97d160f6a7fcd5e48090ecfa90e83981b69c96f144a5aead8bc837e2a3; YD00517437729195%3AWM_TID=PKNVDSDCjDBBQRFBUFJuySoD%2FUsgYQtY; captcha_ticket_v2="2|1:0|10:1625408360|17:captcha_ticket_v2|704:eyJ2YWxpZGF0ZSI6IkNOMzFfN0dTaGhYdFpIS2xYT0RvV05FMnhXZzU2Mmw3OU9RakE2RkQuRGdHekM5MUw3UWgwVDJsU3BTbHkuR3ZTeDdDNDd0SUNZQkh3UkRLdGZlYTJFYlExMnJwWlZaQ0hWRG8xeDJXdHg5Ykx2TzJDMXFheWlHSUU5Zkk4T1hNdnV6VGtJZnRKQ0ZGV00yTm02VlN3MXgxRVpaSnZEc3lITmwuZF9kN3RVU2RPUmVTUFNEWGFOa05GMnIxcWZWZHpJeER0cG5ESDhieGtVTXBEZXZkd0ZmcURsZWFLMmx0LlhEZzBFbUFmWGdGamdBNWFYMlJMOS42ZmE3UHhUN25rNUVHWUp5RFhEZnNtTDk0NHVEeDE4SE04Wk5FQmZQZnZKRVZoUmlBTlZVNkxKbG9lQ2pIV25LbmpEOTBya3ZkT0NsODY0aTJJSFhfUkU1WndHcUFGUUdWYUFmSDlGWDRRbFhwUUswY28tb2c4UE5SNTdsdlZoTVB2WFByVWZNVURaQmFhakpYZUJIcE5hS2gtMWktcHBRMHBKOGpZYnlFR3Rta3dob3FPd0hBN0dMQjlNU1pVekJMdFRlZmxBU2FiYk1qMnBrZm5SWUdCdzVFLktxOHpYTVExLS5fcExybHRoejJnWktxMFNqZm4wei1ZZURQMi1JWGdnU0Y4ZGpYMyJ9|9858648b1c853c19585ca7d22d3612581740390e0ae1a29d5f2790ea3c327e8f"; z_c0="2|1:0|10:1625408360|4:z_c0|92:Mi4xQ244aUJRQUFBQUFBY0Y1c2VJeGNFeVlBQUFCZ0FsVk5hQkhQWVFCQ21IcFR0TGRGbFNjUEZCWVR1c3R4YUwzUHh3|af14287cfce3cf474dd8d09fa72514ba945ba6c85c25e9629aa0ce5d5e89922d"; KLBRSID=031b5396d5ab406499e2ac6fe1bb1a43|1625408361|1625408350; tst=r'
}



s = requests.Session()
for cookie in cookies:
    s.cookies.set(cookie['name'], cookie['value'])

url = 'https://www.zhihu.com/api/v4/me?include=ad_type%2Cavailable_message_types%2Cdefault_notifications_count%2Cfollow_notifications_count%2Cvote_thank_notifications_count%2Cmessages_count%2Cdraft_count%2Cemail%2Caccount_status%2Cis_bind_phone%2Cfollowing_question_count%2Cis_force_renamed%2Crenamed_fullname'
r = s.get(url, headers=headers)
print(r.json())
