from http.cookies import SimpleCookie

cookies = [{'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'KLBRSID', 'path': '/', 'secure': False,
            'value': '031b5396d5ab406499e2ac6fe1bb1a43|1625408366|1625408350'},
           {'domain': '.zhihu.com', 'httpOnly': False, 'name': 'Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49', 'path': '/',
            'secure': False, 'value': '1625408362'},
           {'domain': '.zhihu.com', 'expiry': 1628000362, 'httpOnly': False, 'name': 'tst', 'path': '/',
            'secure': False, 'value': 'r'},
           {'domain': 'www.zhihu.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'YD00517437729195%3AWM_NI',
            'path': '/', 'secure': False,
            'value': 'pWXlPsBhokjGHssEFlBJKsfnM8ww3XWVK5a9LeTEpUg1SsNEZ5qJMDmV%2FW9VjK7fKL%2BO5F%2Buo5dlALI%2BmgbKj8ZO9mkgDLwjaHEzv5AE4vRT6y44atL46oe3CjTurHgrcjE%3D'},
           {'domain': '.zhihu.com', 'expiry': 1628000358.075632, 'httpOnly': True, 'name': 'captcha_ticket_v2',
            'path': '/', 'secure': False,
            'value': '"2|1:0|10:1625408360|17:captcha_ticket_v2|704:eyJ2YWxpZGF0ZSI6IkNOMzFfN0dTaGhYdFpIS2xYT0RvV05FMnhXZzU2Mmw3OU9RakE2RkQuRGdHekM5MUw3UWgwVDJsU3BTbHkuR3ZTeDdDNDd0SUNZQkh3UkRLdGZlYTJFYlExMnJwWlZaQ0hWRG8xeDJXdHg5Ykx2TzJDMXFheWlHSUU5Zkk4T1hNdnV6VGtJZnRKQ0ZGV00yTm02VlN3MXgxRVpaSnZEc3lITmwuZF9kN3RVU2RPUmVTUFNEWGFOa05GMnIxcWZWZHpJeER0cG5ESDhieGtVTXBEZXZkd0ZmcURsZWFLMmx0LlhEZzBFbUFmWGdGamdBNWFYMlJMOS42ZmE3UHhUN25rNUVHWUp5RFhEZnNtTDk0NHVEeDE4SE04Wk5FQmZQZnZKRVZoUmlBTlZVNkxKbG9lQ2pIV25LbmpEOTBya3ZkT0NsODY0aTJJSFhfUkU1WndHcUFGUUdWYUFmSDlGWDRRbFhwUUswY28tb2c4UE5SNTdsdlZoTVB2WFByVWZNVURaQmFhakpYZUJIcE5hS2gtMWktcHBRMHBKOGpZYnlFR3Rta3dob3FPd0hBN0dMQjlNU1pVekJMdFRlZmxBU2FiYk1qMnBrZm5SWUdCdzVFLktxOHpYTVExLS5fcExybHRoejJnWktxMFNqZm4wei1ZZURQMi1JWGdnU0Y4ZGpYMyJ9|9858648b1c853c19585ca7d22d3612581740390e0ae1a29d5f2790ea3c327e8f"'},
           {'domain': 'www.zhihu.com', 'expiry': 1656944353, 'httpOnly': False, 'name': '__snaker__id', 'path': '/',
            'secure': False, 'value': 'Z0Li8ndCCQvYgn84'},
           {'domain': 'www.zhihu.com', 'expiry': 1783089253, 'httpOnly': False, 'name': '_9755xjdesxxd_', 'path': '/',
            'secure': False, 'value': '32'},
           {'domain': '.zhihu.com', 'expiry': 1640960358.841781, 'httpOnly': True, 'name': 'z_c0', 'path': '/',
            'secure': True,
            'value': '"2|1:0|10:1625408360|4:z_c0|92:Mi4xQ244aUJRQUFBQUFBY0Y1c2VJeGNFeVlBQUFCZ0FsVk5hQkhQWVFCQ21IcFR0TGRGbFNjUEZCWVR1c3R4YUwzUHh3|af14287cfce3cf474dd8d09fa72514ba945ba6c85c25e9629aa0ce5d5e89922d"'},
           {'domain': 'www.zhihu.com', 'expiry': 1783089253, 'httpOnly': False, 'name': 'gdxidpyhxdE', 'path': '/',
            'secure': False,
            'value': 'Da2Rdz%2Bb%2F0Dg7ZrO%2FHYc4aZQrwPpKMkAjJ0XI0uVjid1CgtXLLeMVW43njYB1YkvfnJpw4cx0Adfd51Rgp%2FDw8N3Caco4YO3ZbXGxEVvyA4vGNNonGOv3goxyeDnh09V%2Fs6Squ0Od4Z%5Cz1%5C6I%2BtTzyJ60w9UyojX%2BfCvANluyYEXzAHv%3A1625409253954'},
           {'domain': 'www.zhihu.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'YD00517437729195%3AWM_TID',
            'path': '/', 'secure': False, 'value': 'PKNVDSDCjDBBQRFBUFJuySoD%2FUsgYQtY'},
           {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'JOID', 'path': '/', 'secure': False,
            'value': 'VFwTA0tuiYco8ITIV2g9ml6PDpBECbr3TrDyumUO28xFwviuOQxOYE70hMxRRXAyFqBDZT-9oVWTif9uNXjxjAk='},
           {'domain': '.zhihu.com', 'expiry': 1628000351.639749, 'httpOnly': True, 'name': 'captcha_session_v2',
            'path': '/', 'secure': False,
            'value': '"2|1:0|10:1625408353|18:captcha_session_v2|88:NnJoZ2V5TVpiSFA1eklGQUM3clEwUlRKdStoRzU2emJpajhzOTZVRmdOQkgvK1ZUTDlIM0pqcjA4MmtUVFQ0cA==|78d054ace1eb42c609ae7c73c6313ca4ad32c40f66479d800879a5bd79620e77"'},
           {'domain': '.zhihu.com', 'expiry': 1656944362, 'httpOnly': False,
            'name': 'Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49', 'path': '/', 'secure': False, 'value': '1625408351'},
           {'domain': '.zhihu.com', 'expiry': 1720016351.472959, 'httpOnly': False, 'name': 'd_c0', 'path': '/',
            'secure': False, 'value': '"AHBebHiMXBOPTlz164s7LGYGcpC68dYokHU=|1625408353"'},
           {'domain': 'www.zhihu.com', 'expiry': 2147483647, 'httpOnly': False, 'name': 'YD00517437729195%3AWM_NIKE',
            'path': '/', 'secure': False,
            'value': '9ca17ae2e6ffcda170e2e6eeb9c74082ec8b82aa50e9ac8eb2d85a938b8babae6798ad9c92d479bc979ad5c92af0fea7c3b92ab489988dd37982b8a18ce94d8cadbda6c56882968594e5668fe8b998b254958a85d6e43390bb9ca6f35fa5ef9d94cd4f9aebbfabd050af8ae1b8bc5eaa95f987ca48e991fba7aa7fadebac8dc4488bf5a388e141aae896b0bc67a9b98cadf662a3988b9bf53f839bfc97d160f6a7fcd5e48090ecfa90e83981b69c96f144a5aead8bc837e2a3'},
           {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'osd', 'path': '/', 'secure': False,
            'value': 'VV8cBk5viogt9YXLWG04m12AC5VFCrXyS7HxtWAL2s9Kx_2vOgNLZU_3i8lURHM9E6VCZjC4pFSQhvprNHv-iQw='},
           {'domain': 'www.zhihu.com', 'httpOnly': False, 'name': 'SESSIONID', 'path': '/', 'secure': False,
            'value': 'e1vfFtp4fPIk2zdlZMJYlbea9JonbhhOdJPvbMJiKXH'},
           {'domain': '.zhihu.com', 'httpOnly': False, 'name': '_xsrf', 'path': '/', 'secure': False,
            'value': '1f90c97a-53bc-444b-ae9b-48119a07cfea'},
           {'domain': '.zhihu.com', 'expiry': 1688480348.770674, 'httpOnly': False, 'name': '_zap', 'path': '/',
            'secure': False, 'value': 'a360a06a-863b-4cba-b29c-5cd72cf0d127'}]

ori_cookies = '_zap=a360a06a-863b-4cba-b29c-5cd72cf0d127; _xsrf=1f90c97a-53bc-444b-ae9b-48119a07cfea; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1625408351; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1625408351; d_c0="AHBebHiMXBOPTlz164s7LGYGcpC68dYokHU=|1625408353"; captcha_session_v2="2|1:0|10:1625408353|18:captcha_session_v2|88:NnJoZ2V5TVpiSFA1eklGQUM3clEwUlRKdStoRzU2emJpajhzOTZVRmdOQkgvK1ZUTDlIM0pqcjA4MmtUVFQ0cA==|78d054ace1eb42c609ae7c73c6313ca4ad32c40f66479d800879a5bd79620e77"; SESSIONID=e1vfFtp4fPIk2zdlZMJYlbea9JonbhhOdJPvbMJiKXH; JOID=VFwTA0tuiYco8ITIV2g9ml6PDpBECbr3TrDyumUO28xFwviuOQxOYE70hMxRRXAyFqBDZT-9oVWTif9uNXjxjAk=; osd=VV8cBk5viogt9YXLWG04m12AC5VFCrXyS7HxtWAL2s9Kx_2vOgNLZU_3i8lURHM9E6VCZjC4pFSQhvprNHv-iQw=; __snaker__id=Z0Li8ndCCQvYgn84; gdxidpyhxdE=Da2Rdz%2Bb%2F0Dg7ZrO%2FHYc4aZQrwPpKMkAjJ0XI0uVjid1CgtXLLeMVW43njYB1YkvfnJpw4cx0Adfd51Rgp%2FDw8N3Caco4YO3ZbXGxEVvyA4vGNNonGOv3goxyeDnh09V%2Fs6Squ0Od4Z%5Cz1%5C6I%2BtTzyJ60w9UyojX%2BfCvANluyYEXzAHv%3A1625409253954; _9755xjdesxxd_=32; YD00517437729195%3AWM_NI=pWXlPsBhokjGHssEFlBJKsfnM8ww3XWVK5a9LeTEpUg1SsNEZ5qJMDmV%2FW9VjK7fKL%2BO5F%2Buo5dlALI%2BmgbKj8ZO9mkgDLwjaHEzv5AE4vRT6y44atL46oe3CjTurHgrcjE%3D; YD00517437729195%3AWM_NIKE=9ca17ae2e6ffcda170e2e6eeb9c74082ec8b82aa50e9ac8eb2d85a938b8babae6798ad9c92d479bc979ad5c92af0fea7c3b92ab489988dd37982b8a18ce94d8cadbda6c56882968594e5668fe8b998b254958a85d6e43390bb9ca6f35fa5ef9d94cd4f9aebbfabd050af8ae1b8bc5eaa95f987ca48e991fba7aa7fadebac8dc4488bf5a388e141aae896b0bc67a9b98cadf662a3988b9bf53f839bfc97d160f6a7fcd5e48090ecfa90e83981b69c96f144a5aead8bc837e2a3; YD00517437729195%3AWM_TID=PKNVDSDCjDBBQRFBUFJuySoD%2FUsgYQtY; captcha_ticket_v2="2|1:0|10:1625408360|17:captcha_ticket_v2|704:eyJ2YWxpZGF0ZSI6IkNOMzFfN0dTaGhYdFpIS2xYT0RvV05FMnhXZzU2Mmw3OU9RakE2RkQuRGdHekM5MUw3UWgwVDJsU3BTbHkuR3ZTeDdDNDd0SUNZQkh3UkRLdGZlYTJFYlExMnJwWlZaQ0hWRG8xeDJXdHg5Ykx2TzJDMXFheWlHSUU5Zkk4T1hNdnV6VGtJZnRKQ0ZGV00yTm02VlN3MXgxRVpaSnZEc3lITmwuZF9kN3RVU2RPUmVTUFNEWGFOa05GMnIxcWZWZHpJeER0cG5ESDhieGtVTXBEZXZkd0ZmcURsZWFLMmx0LlhEZzBFbUFmWGdGamdBNWFYMlJMOS42ZmE3UHhUN25rNUVHWUp5RFhEZnNtTDk0NHVEeDE4SE04Wk5FQmZQZnZKRVZoUmlBTlZVNkxKbG9lQ2pIV25LbmpEOTBya3ZkT0NsODY0aTJJSFhfUkU1WndHcUFGUUdWYUFmSDlGWDRRbFhwUUswY28tb2c4UE5SNTdsdlZoTVB2WFByVWZNVURaQmFhakpYZUJIcE5hS2gtMWktcHBRMHBKOGpZYnlFR3Rta3dob3FPd0hBN0dMQjlNU1pVekJMdFRlZmxBU2FiYk1qMnBrZm5SWUdCdzVFLktxOHpYTVExLS5fcExybHRoejJnWktxMFNqZm4wei1ZZURQMi1JWGdnU0Y4ZGpYMyJ9|9858648b1c853c19585ca7d22d3612581740390e0ae1a29d5f2790ea3c327e8f"; z_c0="2|1:0|10:1625408360|4:z_c0|92:Mi4xQ244aUJRQUFBQUFBY0Y1c2VJeGNFeVlBQUFCZ0FsVk5hQkhQWVFCQ21IcFR0TGRGbFNjUEZCWVR1c3R4YUwzUHh3|af14287cfce3cf474dd8d09fa72514ba945ba6c85c25e9629aa0ce5d5e89922d"; KLBRSID=031b5396d5ab406499e2ac6fe1bb1a43|1625408361|1625408350; tst=r'
ori_cookies2 = '_zap=a360a06a-863b-4cba-b29c-5cd72cf0d127; _xsrf=1f90c97a-53bc-444b-ae9b-48119a07cfea; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1625408351; d_c0="AHBebHiMXBOPTlz164s7LGYGcpC68dYokHU=|1625408353"; captcha_session_v2="2|1:0|10:1625408353|18:captcha_session_v2|88:NnJoZ2V5TVpiSFA1eklGQUM3clEwUlRKdStoRzU2emJpajhzOTZVRmdOQkgvK1ZUTDlIM0pqcjA4MmtUVFQ0cA==|78d054ace1eb42c609ae7c73c6313ca4ad32c40f66479d800879a5bd79620e77"; captcha_ticket_v2="2|1:0|10:1625408360|17:captcha_ticket_v2|704:eyJ2YWxpZGF0ZSI6IkNOMzFfN0dTaGhYdFpIS2xYT0RvV05FMnhXZzU2Mmw3OU9RakE2RkQuRGdHekM5MUw3UWgwVDJsU3BTbHkuR3ZTeDdDNDd0SUNZQkh3UkRLdGZlYTJFYlExMnJwWlZaQ0hWRG8xeDJXdHg5Ykx2TzJDMXFheWlHSUU5Zkk4T1hNdnV6VGtJZnRKQ0ZGV00yTm02VlN3MXgxRVpaSnZEc3lITmwuZF9kN3RVU2RPUmVTUFNEWGFOa05GMnIxcWZWZHpJeER0cG5ESDhieGtVTXBEZXZkd0ZmcURsZWFLMmx0LlhEZzBFbUFmWGdGamdBNWFYMlJMOS42ZmE3UHhUN25rNUVHWUp5RFhEZnNtTDk0NHVEeDE4SE04Wk5FQmZQZnZKRVZoUmlBTlZVNkxKbG9lQ2pIV25LbmpEOTBya3ZkT0NsODY0aTJJSFhfUkU1WndHcUFGUUdWYUFmSDlGWDRRbFhwUUswY28tb2c4UE5SNTdsdlZoTVB2WFByVWZNVURaQmFhakpYZUJIcE5hS2gtMWktcHBRMHBKOGpZYnlFR3Rta3dob3FPd0hBN0dMQjlNU1pVekJMdFRlZmxBU2FiYk1qMnBrZm5SWUdCdzVFLktxOHpYTVExLS5fcExybHRoejJnWktxMFNqZm4wei1ZZURQMi1JWGdnU0Y4ZGpYMyJ9|9858648b1c853c19585ca7d22d3612581740390e0ae1a29d5f2790ea3c327e8f"; z_c0="2|1:0|10:1625408360|4:z_c0|92:Mi4xQ244aUJRQUFBQUFBY0Y1c2VJeGNFeVlBQUFCZ0FsVk5hQkhQWVFCQ21IcFR0TGRGbFNjUEZCWVR1c3R4YUwzUHh3|af14287cfce3cf474dd8d09fa72514ba945ba6c85c25e9629aa0ce5d5e89922d"; tst=r; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1625408362; KLBRSID=f48cb29c5180c5b0d91ded2e70103232|1625408366|1625408365'

new_cookies = {}
for c in cookies:
    new_cookies[c['name']] = c['value']


def cookie_str2dict(ori_cookies):
    cookie = SimpleCookie()
    cookie.load(ori_cookies)

    cookies = {}
    for key, morsel in cookie.items():
        cookies[key] = morsel.value

    return cookies


def print_dict(key_value):
    for i in sorted(key_value):
        print((i, key_value[i]), end=" ")
    print('\n')


ori_cookies = cookie_str2dict(ori_cookies)
ori_cookies2 = cookie_str2dict(ori_cookies2)

print_dict(new_cookies)

print_dict(ori_cookies)
print_dict(ori_cookies2)
